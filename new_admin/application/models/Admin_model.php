<?php

class Admin_model extends CI_Model {
	public function get_data($where = '', $table, $select = '')
	{
		if($where!=''){
            $this->db->where($where);
        }
        if($select != ''){
            $this->db->select($select);
        }
        return $this->db->get($table)->result_array();
    }
    public function submit($table, $data){
        if($this->db->insert($table, $data)){
            return true;
        }else{
            return false;
        }
    }
    public function get_update($table, $data, $where){
        $this->db->where($where);
        return $this->db->update($table, $data);
    }
}