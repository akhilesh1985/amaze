<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    public function __construct(){
        parent::__construct();
        if(empty($this->session->userdata('user_name'))){
            header('Location: '.base_url());
            exit;
        }
        $this->load->library('pagination');
        $this->load->model('loginmodel');
        $this->load->model('admin_model');
    }
    public function index(){
        $this->load->view('admin/layout/header');
        $this->load->view('admin/layout/sidebar');
        $this->load->view('admin/dashboard');
        $this->load->view('admin/layout/footer');
    }
    public function student(){
        $this->load->view('admin/layout/header');
        $this->load->view('admin/layout/sidebar');
        $this->load->view('admin/student');
    }
    public function video(){
        $this->load->view('admin/layout/header');
        $this->load->view('admin/layout/sidebar');
        $this->load->view('admin/video');
        
    }
    public function student_status(){
        $type = $this->input->get_post('type');
        $id = $this->input->get_post('id');
        $data['isactivated'] = $type;
        $this->admin_model->get_update('admin', $data, 'admin_id = '.$id.'');
    }
    public function video_add(){
        if(isset($_POST['submit'])){
            $data['title'] = $this->input->get_post('title');
            $data['discription'] = $this->input->get_post('description');
            $data['link'] = $this->input->get_post('link');
            $this->form_validation->set_rules('title','Please Enter Title','required|trim');
			$this->form_validation->set_rules('description','Please Enter Description','required|trim');
            $this->form_validation->set_rules('link','Please Enter Link','required|trim');
            if($this->form_validation->run('add_bank')){
                if($this->admin_model->submit('video', $data)){
                    redirect(base_url('admin/video'));
                }else{
                    $this->load->view('admin/layout/header');
                    $this->load->view('admin/layout/sidebar');
                    $this->load->view('admin/video_add');
                    $this->load->view('admin/layout/footer');
                }
            }else{
                $this->load->view('admin/layout/header');
                $this->load->view('admin/layout/sidebar');
                $this->load->view('admin/video_add');
                $this->load->view('admin/layout/footer');
            }
        }else{
            $this->load->view('admin/layout/header');
            $this->load->view('admin/layout/sidebar');
            $this->load->view('admin/video_add');
            $this->load->view('admin/layout/footer');
        }
    }
    public function lanuage(){
        $this->load->view('admin/layout/header');
        $this->load->view('admin/layout/sidebar');
        $this->load->view('admin/language');
        $this->load->view('admin/layout/footer');
    }
    public function menu(){
        $this->load->view('admin/layout/header');
        $this->load->view('admin/layout/sidebar');
        $this->load->view('admin/menu');
        $this->load->view('admin/layout/footer');
    }
    public function menu_add(){
        $this->load->view('admin/layout/header');
        $this->load->view('admin/layout/sidebar');
        $this->load->view('admin/menu_add');
        $this->load->view('admin/layout/footer');
    }
}
