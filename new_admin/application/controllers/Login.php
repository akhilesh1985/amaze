<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('loginmodel');
    }
    public function index(){
        $this->load->view('login');
    }
    public function admin_login(){
        if($_REQUEST['username']!='' && $_REQUEST['password']!='')
		{
			$username = $_REQUEST['username'];
			$password = $_REQUEST['password'];	
			if($login_detail = $this->loginmodel->login_valid($username, $password))
			{
				$login_id = $login_detail->admin_id;
				$login_name = $login_detail->fname;
				$status = $login_detail->type;
				//$hspid = $login_detail->hospital_id;
				$this->session->set_userdata('user_id', $login_id);
				$this->session->set_userdata('user_name', $login_name);
				$this->session->set_userdata('type', $status);
				$error = '0';
			}
			else
			{
				$error = '1';
			}
		}
		else
		{
			$error= '1';
		}
		echo json_encode(array('error'=>$error));
    }
    public function logout(){
        session_unset();
        session_destroy();
        redirect(base_url());
    }
}
