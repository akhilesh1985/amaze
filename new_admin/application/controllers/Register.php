<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('registermodel');
        $this->load->model('admin_model');
    }
    public function index(){
        if(isset($_POST['submit'])){
           $data['fname'] = $this->input->get_post('name');
           $data['mobile'] = $this->input->get_post('mobile');
           $data['email'] = $this->input->get_post('email');
           $data['password'] = md5($this->input->get_post('password'));
           $conpassword = $this->input->get_post('conpassword');
           $data['education'] = md5($this->input->get_post('education'));
           $data['state'] = md5($this->input->get_post('state'));
           $data['dob'] = md5($this->input->get_post('dob'));
           $data['type'] = '2';
           $data['isactivated'] = '1';
           $data['created_on'] = date('Y-m-d h:i:s');
           $confirm_pass = $this->input->get_post('confirm_pass');
           $this->form_validation->set_rules('name','Please Enter Name','required|trim');
			$this->form_validation->set_rules('mobile','Please Enter mobile no','required|trim|numeric');
            $this->form_validation->set_rules('email','Please Enter email','required|trim');
            $this->form_validation->set_rules('education','Please Enter Education','required|trim');
            $this->form_validation->set_rules('state','Please Enter Sate','required|trim');
            $this->form_validation->set_rules('dob','Please Enter Date of Birth','required|trim');
            $this->form_validation->set_rules('password','Please Enter password','required|trim');
            $this->form_validation->set_rules('conpassword','Please Enter Confirm Password','required|trim');
			if($this->form_validation->run('add_bank')){
                if($conpassword == $data['password']){
                    $this->session->set_flashdata('add_bank','Confirm password and password should be same.');
                    $this->load->view('register');
                }else{
                    if($this->admin_model->get_data('email = "'.$data['email'].'"','admin')){
                        
                            $this->session->set_flashdata('add_bank','Email already exist.');
                            $this->load->view('register');
                    }else{
                        $this->registermodel->add($data);
                        $this->session->set_flashdata('add_bank','Refistration successfull.');
                        $this->load->view('register');
                    }
                }
            }else{
                $this->load->view('register');
            }
        }else{
            $this->load->view('register');
        }
    }
}
