<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?= base_url('admin') ?>" class="brand-link">
      <img src="<?= base_url('assets') ?>/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Admin Panel</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?= base_url('assets') ?>/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?= $_SESSION['user_name']?></a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <?php 
          $role = $this->admin_model->get_data('id = '.$_SESSION['type'].'','role');
          $menu = $this->admin_model->get_data('isactive = "1" AND supermenu_id = "0" AND FIND_IN_SET(id, "'.$role[0]['menus'].'")', 'menu');
          //print_r($menu);
          foreach($menu as $me){
            $sub_menu = $this->admin_model->get_data('isactive = "1" AND supermenu_id = "'.$me['id'].'" AND FIND_IN_SET(id, "'.$role[0]['menus'].'")', 'menu');
            $menu_count = count($sub_menu);
          ?>
           <li class="nav-item menu-open">
            <a href="<?= base_url($me['link']); ?>" class="nav-link active">
              <i class="<?= $me['fa_fa_icon'] ?>"></i>
              <p>
                <?= $me['name'] ?>
                <i class="<?= $me['fa_fa_icon_back'] ?>"></i>
                <span class="badge badge-info right"><?= $menu_count ?></span>
              </p>
            </a>
            <?php 
            if($menu_count != '0'){
            ?>
            <ul class="nav nav-treeview">
              <?php 
              foreach($sub_menu as $sub){ ?>
              <li class="nav-item">
                <a href="<?= base_url($sub['link']) ?>" class="nav-link">
                  <i class="<?= $sub['fa_fa_icon'] ?>"></i>
                  <p><?= $sub['name'] ?></p>
                </a>
              </li>
              <?php } ?>
            </ul>
            <?php } ?>
          </li>
          <?php } ?>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
