<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Videos</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Videos</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

     <!-- Main content -->
     <section class="content">
      <div class="container-fluid">
        <!-- <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header" aling="right">
                <!-- <div class="col-lg-6">
                <h3 class="card-title">DataTable with minimal features & hover style</h3>
                </div> -->
                <div class="col-lg-6">
                    <div class="btn-group w-20">
                    <?php if($_SESSION['type'] == 1){ ?>
                      <a href="<?= base_url('admin/video_add')?>" class="btn btn-success col fileinput-button">
                        <i class="fas fa-plus"></i>
                        <span>Add Video</span>
                      </a>
                    <?php } ?>
                      <!-- <button type="submit" class="btn btn-primary col start">
                        <i class="fas fa-upload"></i>
                        <span>Start upload</span>
                      </button>
                      <button type="reset" class="btn btn-warning col cancel">
                        <i class="fas fa-times-circle"></i>
                        <span>Cancel upload</span>
                      </button> -->
                    </div>
                  </div>
              </div>
              <!-- /.card-header -->
              <!-- <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>Title</th>
                    <th>Discription</th>
                    <th>Link</th>
                  </tr>
                  </thead>
                  <tbody> -->
                <?php $data = $this->admin_model->get_data('','video'); 
                foreach($data as $da){
                ?>
                  <!-- <tr>
                    <td><?= $da['title'] ?></td>
                    <td><?= $da['discription'] ?></td>
                    <td><iframe src="<?= $da['link']?>" width="50%" height="100" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></td>
                  </tr> -->
                <?php } ?>
                  <!-- </tbody>
                  
                </table>
              </div> -->
              <!-- <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div> -->
           <!-- </div>
        </div> -->
        <!-- <div class="card card-success">
          <div class="card-body"> -->
            <div class="row">
            <?php $data = $this->admin_model->get_data('','video'); ?>
              
              <?php   foreach($data as $da){ ?>
                <div class="col-md-12 col-lg-6 col-xl-4">
                <iframe src="<?= $da['link']?>" width="100%" height="260" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                <div>
                  <p><?= $da['title'] ?></p>
                </div>
                </div>
                <?php } ?>
            </div>
          <!-- </div>
        </div> -->
      </div>
    </section>
            <!-- /.card -->
<?php 
    $this->load->view('admin/layout/footer');
?>
<script src="<?= base_url('assets') ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets') ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url('assets') ?>/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url('assets') ?>/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?= base_url('assets') ?>/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url('assets') ?>/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?= base_url('assets') ?>/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?= base_url('assets') ?>/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?= base_url('assets') ?>/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>