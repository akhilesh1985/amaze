<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Student List</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Student List</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

     <!-- Main content -->
     <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header" aling="right">
                <!-- <div class="col-lg-6">
                <h3 class="card-title">DataTable with minimal features & hover style</h3>
                </div> -->
                <div class="col-lg-6">
                    <div class="btn-group w-20">
                      <!-- <a href="<?= base_url('admin/menu_add')?>" class="btn btn-success col fileinput-button">
                        <i class="fas fa-plus"></i>
                        <span>Add Menu</span>
                      </a> -->
                      <!-- <button type="submit" class="btn btn-primary col start">
                        <i class="fas fa-upload"></i>
                        <span>Start upload</span>
                      </button>
                      <button type="reset" class="btn btn-warning col cancel">
                        <i class="fas fa-times-circle"></i>
                        <span>Cancel upload</span>
                      </button> -->
                    </div>
                  </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Registeration Date</th>
                    <th>Options</th>
                  </tr>
                  </thead>
                  <tbody>
                <?php $data = $this->admin_model->get_data('type=2','admin'); 
                foreach($data as $da){
                ?>
                  <tr>
                    <td><?= $da['fname'] ?></td>
                    <td><?= $da['email'] ?></td>
                    <td><?= $da['mobile']?></td>
                    <td><?= $da['created_on']?></td>
                    <td><?php 
                    if($da['isactivated'] == 1){
                      echo "<button onclick='status(".$da['admin_id'].", 0)' class='btn btn-danger'>Deactive</button>";
                    }else{
                      echo "<button onclick='status(".$da['admin_id'].", 1)' class='btn btn-success'>Active</button>";
                    }
                    ?>
                  </td>
                  </tr>
                <?php } ?>
                  </tbody>
                  
                </table>
              </div>
              <!-- <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div> -->
           </div>
        </div>
      </div>
    </section>
            <!-- /.card -->
<?php 
    $this->load->view('admin/layout/footer');
?>
<script src="<?= base_url('assets') ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets') ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url('assets') ?>/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url('assets') ?>/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?= base_url('assets') ?>/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url('assets') ?>/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?= base_url('assets') ?>/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?= base_url('assets') ?>/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?= base_url('assets') ?>/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script>
  function status(id, type){
    $.ajax({
      url: "<?= base_url('admin/student_status')?>?type="+type+"&id="+id,
      cache: false,
      success: function(html){
        location.reload();
      }
    });
  }
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>