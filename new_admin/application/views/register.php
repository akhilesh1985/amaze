<!DOCTYPE html>
<html lang="zxx">

<title>Arms Incorporation TITP | H&A India Pvt.Ltd.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <!-- External CSS libraries -->
    <link type="text/css" rel="stylesheet" href="<?= base_url('assets') ?>/assets/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url('assets') ?>/assets/fonts/font-awesome/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url('assets') ?>/assets/fonts/flaticon/font/flaticon.css">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="https://www.arms-incorporation.com/images/favicon.png" type="image/x-icon" >

    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPoppins:400,500,700,800,900%7CRoboto:100,300,400,400i,500,700">
    <link href="https://fonts.googleapis.com/css2?family=Jost:wght@300;400;500;600;700;800;900&amp;display=swap" rel="stylesheet">

    <!-- Custom Stylesheet -->
    <link type="text/css" rel="stylesheet" href="<?= base_url('assets') ?>/assets/css/style.css">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="<?= base_url('assets') ?>/assets/css/skins/default.css">

</head>
<body id="top">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TAGCODE"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="page_loader"></div>

<!-- Login 2 start -->
<div class="login-2">
    <div class="container-fluid">
        <div class="row login-box">
            <div class="col-lg-6 align-self-center pad-0 form-section">
                <div class="form-inner">
                    <img src="<?= base_url('assets') ?>/assets/img/leftside-image.svg" alt="logo" class="w-100 img-fluid">
                    <a href="index.html" class="logo">
                         <img src="https://www.arms-incorporation.com/images/logo.png" alt="logo">
                    </a>
                    <h3>Create an account</h3>
                    <?php if( $error = $this->session->flashdata('add_bank')){ ?>
                            <diV class="col-md-3"></div>
                            <div class="col-md-9 corm_nmset">
                                <div class=" error" style="margin-left:0%;">
                                    <?= $error ?>
                                </div>
                            </div>
                    <?php } ?>
                    <form action="register" method="GET">
                        <div class="form-group form-box">
                        <?php echo form_error('name'); ?>
                            <input type="text" name="name" class="input-text" placeholder="Full Name">
                        </div>
                         <div class="form-group form-box">
                         <?php echo form_error('mobile'); ?>
                            <input type="text" name="mobile" class="input-text" placeholder="Mobile Number">
                        </div>
                        <div class="form-group form-box">
                        <?php echo form_error('email'); ?>
                            <input type="email" name="email" class="input-text" placeholder="Email Address">
                        </div>
                        <div class="form-group form-box">
                        <?php echo form_error('education'); ?>
                            <select name="education" class="input-text">
                                <option value="">Education Qualification</option>
                                <option value="High School">High School</option>
                                <option value="Degree">Degree</option>
                                <option value="PG/Master’s">PG/Master’s</option>
                            </select>
                        </div>
                        <div class="form-group form-box">
                        <?php echo form_error('state'); ?>
                            <input type="text" name="state" class="input-text" placeholder="State">
                        </div>
                        <div class="form-group form-box">
                        <?php echo form_error('dob'); ?>
                            <input type="text" name="dob" class="input-text" placeholder="Date of Birth">
                        </div>
                        <div class="form-group form-box clearfix">
                        <?php echo form_error('password'); ?>
                            <input type="password" name="Password" class="input-text" placeholder="Password">
                        </div>
                        <div class="form-group form-box clearfix">
                        <?php echo form_error('conpassword'); ?>
                            <input type="password" name="conpassword" class="input-text" placeholder="Confirm Password">
                        </div>
                        <!-- <div class="form-group checkbox clearfix">
                            <div class="checkbox clearfix">
                                <div class="form-check checkbox-theme">
                                    <input class="form-check-input" type="checkbox" value="" id="rememberMe">
                                    <label class="form-check-label" for="rememberMe">
                                        I agree to the<a href="#" class="terms">terms of service</a>
                                    </label>
                                </div>
                            </div>
                        </div> -->
                        <div class="form-group clearfix">
                            <button type="submit" name="submit" class="btn-md btn-theme btn-block">Login</button>
                        </div>
                        <div class="extra-login clearfix">
                            <span>Or Login With</span>
                        </div>
                    </form>
                    <div class="clearfix"></div>
                    <p>Already a member? <a href="login">Login here</a></p>
                </div>
            </div>
            <div class="col-lg-6 bg-color-15 align-self-center pad-0 none-992 bg-img">
                <img src="<?= base_url('assets') ?>/assets/img/img-3.svg" alt="logo" class="w-100 img-fluid">
            </div>
        </div>
    </div>
</div>
<!-- Login 2 end -->

<!-- External JS libraries -->
<script src="<?= base_url('assets') ?>/assets/js/jquery-2.2.0.min.js"></script>
<script src="<?= base_url('assets') ?>/assets/js/popper.min.js"></script>
<script src="<?= base_url('assets') ?>/assets/js/bootstrap.min.js"></script>
<!-- Custom JS Script -->

</body>

<!-- Mirrored from storage.googleapis.com/theme-vessel-items/checking-sites/logdy-html/HTML/main/register-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 01 Jun 2021 06:07:59 GMT -->
</html>