<!DOCTYPE html>
<html lang="zxx">

<head>
    
     <title>Arms Incorporation TITP | H&A India Pvt.Ltd.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <!-- External CSS libraries -->
    <link type="text/css" rel="stylesheet" href="<?= base_url('assets') ?>/assets/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url('assets') ?>/assets/fonts/font-awesome/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url('assets') ?>/assets/fonts/flaticon/font/flaticon.css">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="https://www.arms-incorporation.com/images/favicon.png" type="image/x-icon" >

    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPoppins:400,500,700,800,900%7CRoboto:100,300,400,400i,500,700">
    <link href="https://fonts.googleapis.com/css2?family=Jost:wght@300;400;500;600;700;800;900&amp;display=swap" rel="stylesheet">

    <!-- Custom Stylesheet -->
    <link type="text/css" rel="stylesheet" href="<?= base_url('assets') ?>/assets/css/style.css">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="<?= base_url('assets') ?>/assets/css/skins/default.css">

</head>
<body id="top">


<!-- Login section start -->
<div class="login-2">
    <div class="container-fluid">
        <div class="row login-box">
            <div class="col-lg-6 align-self-center pad-0 form-section">
                <div class="form-inner">
                    <img src="<?= base_url('assets') ?>/assets/img/leftside-image.svg" alt="logo" class="w-100 img-fluid">
                    <a href="index.html" class="logo">
                        <img src="https://www.arms-incorporation.com/images/logo.png" alt="logo">
                    </a>
                    <h3>Sign into your account</h3>
                    <?php echo form_open('#', ['id'=>'frm_login']) ?>
                    <?php if( $error = $this->session->flashdata('login_failed')): ?>
                        <div class="callout callout-danger">
                            <?= $error ?>
                        </div>
                    <?php endif; ?>
                    <form action="#" id="frm_login" method="Post">
                        <div class="form-group form-box">
                            <input type="email" name="username" id="uname" class="input-text" placeholder="Email Address">
                        </div>
                        <div class="form-group form-box clearfix">
                                <input type="password" name="password" id="password" class="input-text"  placeholder="Password">
                        </div>
                        <!-- <div class="form-group checkbox clearfix">
                            <div class="form-check checkbox-theme">
                                 <input class="form-check-input" type="checkbox" value="" id="rememberMe" onclick="showpass();">
                                    <label class="form-check-label" for="rememberMe">
                                        View Password
                                    </label>
                            </div>
                            <a href="forgot-password.html" class="forgot-password">Forgot Password</a>
                        </div> -->
                        <div class="form-group clearfix">
                            <button type="submit" id="btnLogin" class="btn-md btn-theme btn-block">Login</button>
                        </div>
                        <div class="extra-login clearfix">
                            <span>Or Login With</span>
                        </div>
                    </form>
                    <div class="clearfix"></div>
                   
                    <p>Don't have an account? <a href="register" class="thembo"> Register here</a></p>
                </div>
            </div>
            <div class="col-lg-6 bg-color-15 align-self-center pad-0 none-992 bg-img">
                <img src="<?= base_url('assets') ?>/assets/img/img-3.svg" alt="logo" class="w-100 img-fluid">
            </div>
        </div>
    </div>
</div>
<!-- Login section end -->

<!-- External JS libraries -->
<script src="<?= base_url('assets') ?>/assets/js/jquery-2.2.0.min.js"></script>
<script src="<?= base_url('assets') ?>/assets/js/popper.min.js"></script>
<script src="<?= base_url('assets') ?>/assets/js/bootstrap.min.js"></script>
<script src="<?= base_url('assets') ?>/assets/js/custom.js"></script>
<script src="<?= base_url('assets/main/js/ajaxloader.js')?>"></script>
<script type="text/javascript">
 $(document).ready(function() {
        $('#btnLogin').on('click',function(event){
                if($('#uname, #password').val()!='')
                  {
                    event.preventDefault();

                       ajaxloader.loadURL("<?php echo base_url('login/admin_login') ?>",$('#frm_login').serialize(), function(resp){
                        var data = resp;
                        if(data['error']=='1'){   
                           $('#frm_login').find('.text-danger').remove();
                           $('.error').before("<p class='text-danger'>Invalid Username/Password.</p>");
                           return false;
                        }
                        // else if(data['error']=='2'){
                        //   window.location="<?php echo base_url('fif'); ?>"
                        // }
                        else{
                             window.location = "<?php echo base_url('admin') ?>";
                        }
                       });
                       return false;
                  }
        });
  });
</script>
<!-- Custom JS Script -->
</body>

</html>